#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QtSerialPort>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <qtimer.h>
#include <qlist.h>

#include "macro.h"


struct sCanMess {
    QString id;
    QString ln;
    QString b0;
    QString b1;
    QString b2;
    QString b3;
    QString b4;
    QString b5;
    QString b6;
    QString b7;
    QTime   current_ts;
};


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);    
    ~MainWindow();

private slots:

    void taskMacro();

    void tickms();
    void setCanSpeed();
    void readData();
    void procRx();
    void refreshRx();

    void on_buttonOpenClosePort_clicked();
    void on_buttonSendMess_clicked();

    void on_check_every_clicked();

    void on_combo_recent_currentIndexChanged(const QString &arg1);

    void on_buttonClearRx_clicked();

    void on_checkFilterId_clicked();

    void on_buttonSaveMacro_clicked();

    void on_buttonAddToMacroList_clicked();

    void on_buttonOpenMacro_clicked();

    void on_buttonStartMacro_clicked();

    void on_buttonClearMacro_clicked();

    void on_buttonReloadComPort_clicked();

    void on_buttoSendRow_clicked();

    void on_buttonClearRow_clicked();

private:
    Ui::MainWindow*  ui;
    QSerialPort*     seriale;
    QSerialPortInfo* info;

    //QTimer*          timer1ms;
    QTimer*          tick;
    QTimer*          timerMacro;
    QTimer*          timerRefresh;

    QSortFilterProxyModel *filterModelRx;

    QStandardItemModel *modelRx;
    QStandardItemModel *modelMacro;
    QByteArray rx;
    QString    tx;
    QString    tx_mem;
    int command;
    int countRx;
    int faseRead;
    int flagRx;

    Macro* macro;
    int current_macro;
    bool macro_run;

    QList<sCanMess> listaRx;

signals:


    void readyRx( QByteArray buff);
};

#endif // MAINWINDOW_H
