#ifndef MACRO_H
#define MACRO_H

#include <qobject.h>
#include <qlist.h>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QStandardItemModel>




struct sMacro
{
    QString tx;
    QString comment;
    int     timeout;
};

class Macro
{
public:
    Macro();

    bool esporta(QString filename);
    bool importa(QString filename);
    void clear();
    void add( QString tx, QString timeout, QString comment );

    int  count()
    {
        return macroModel->rowCount();
    }

    QString tx(int index )
    {
        QModelIndex indexTx = macroModel->index( index ,0 );
        QString tx          = macroModel->data(indexTx).toString();
        return tx;
    }
    int timeout(int index )
    {
        QModelIndex indexTimeout = macroModel->index( index, 1 );

        QString timeout = macroModel->data(indexTimeout).toString();
        return timeout.toInt();
    }
    bool remove( int index)
    {
        return macroModel->removeRows( index, 1 );
    }


    QStandardItemModel* macroModel;

    int current_index;

private:

    QList<sMacro> mMacroList;

    //int mIndex;


};

#endif // MACRO_H
