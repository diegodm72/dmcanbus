#include "macro.h"
#include <qfile.h>
Macro::Macro()
{
    macroModel = new QStandardItemModel( 0,3 );

    macroModel->setHorizontalHeaderItem( 0, new QStandardItem(QString("TX"))     );
    macroModel->setHorizontalHeaderItem( 1, new QStandardItem(QString("TIMEOUT")));
    macroModel->setHorizontalHeaderItem( 2, new QStandardItem(QString("COMMENT")));

    //this->importa("macrotest.json");

//    this->add( "001", "miocommento", "100" );
//    this->add( "002", "miocommento", "200" );
//    this->add( "003", "miocommento", "300" );
//    this->add( "004", "miocommento", "400" );

    //this->esporta( "testSalvataggio.json" );
}

bool Macro::esporta(QString filename)
{
    QJsonObject jDoc;
    QJsonArray  jMacroList;

    macroModel->rowCount();

    for( int p= 0; p < macroModel->rowCount(); p++)
    {
        QModelIndex indexTx      = macroModel->index( p ,0 );
        QModelIndex indexTimeout = macroModel->index( p, 1 );
        QModelIndex indexComment = macroModel->index( p, 2 );

        QString tx      = macroModel->data(indexTx).toString();
        QString comment = macroModel->data(indexComment).toString();
        QString timeout = macroModel->data(indexTimeout).toString();

        QJsonObject jMacro;
        jMacro["tx"]      = tx;
        jMacro["timeout"] = timeout;
        jMacro["comment"] = comment;

        jMacroList.append( jMacro);
    }

    jDoc["can speed"]  = 125000;
    jDoc["macro list"] = jMacroList;

    QJsonDocument doc;
    doc.setObject( jDoc );

    QFile saveFile( filename );

    if (!saveFile.open(QIODevice::WriteOnly))
    {
        qWarning("Couldn't open save file.");
        return false;
    }

    saveFile.write( doc.toJson() );
    saveFile.close();

    return true;

}

bool Macro::importa(QString filename)
{
    macroModel->setRowCount(0);

    QFile loadFile( filename );

    if (!loadFile.open(QIODevice::ReadOnly))
    {
        qWarning("Couldn't open save file.");
        return false;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument doc = QJsonDocument::fromJson(saveData);

    QJsonObject jMacro = doc.object();
    QJsonArray  jMacroList = jMacro["macro list"].toArray();

    for( int p= 0; p < jMacroList.count(); p++ )
    {
        QJsonObject jm = jMacroList.at(p).toObject();
        QString tx      = jm["tx"].toString();
        QString timeout = jm["timeout"].toString();
        QString comment = jm["comment"].toString();

        this->add( tx, timeout, comment);
    }

    return true;
}

void Macro::add(QString tx, QString timeout, QString comment )
{
    QList<QStandardItem*> riga;

    QStandardItem* item_tx      = new QStandardItem( tx );
    QStandardItem* item_timeout = new QStandardItem( timeout );
    QStandardItem* item_comment = new QStandardItem( comment );

    riga.append( item_tx );
    riga.append( item_timeout );
    riga.append( item_comment );

    macroModel->appendRow( riga );

}

void Macro::clear()
{
    macroModel->clear();
}
