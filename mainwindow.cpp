#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    macro = new Macro();
    macro_run = false;


    seriale = new QSerialPort();
    QList<QSerialPortInfo> listaCom = QSerialPortInfo::availablePorts();
    foreach ( QSerialPortInfo port, listaCom )
    {
        //if( port.isValid() )
        //{
            QString loc  = port.systemLocation();
            QString des  = port.description();
            QString nome = port.portName();
            ui->comboPortName->addItem(nome);
        //}
    }

    modelRx = new QStandardItemModel( 0 , 11 ,this ) ; //2 Rows and 3 Columns
    modelRx->setHorizontalHeaderItem(  0, new QStandardItem(QString("ID")));
    modelRx->setHorizontalHeaderItem(  1, new QStandardItem(QString("LEN")));
    modelRx->setHorizontalHeaderItem(  2, new QStandardItem(QString("B0")));
    modelRx->setHorizontalHeaderItem(  3, new QStandardItem(QString("B1")));
    modelRx->setHorizontalHeaderItem(  4, new QStandardItem(QString("B2")));
    modelRx->setHorizontalHeaderItem(  5, new QStandardItem(QString("B3")));
    modelRx->setHorizontalHeaderItem(  6, new QStandardItem(QString("B4")));
    modelRx->setHorizontalHeaderItem(  7, new QStandardItem(QString("B5")));
    modelRx->setHorizontalHeaderItem(  8, new QStandardItem(QString("B6")));
    modelRx->setHorizontalHeaderItem(  9, new QStandardItem(QString("B7")));
    modelRx->setHorizontalHeaderItem( 10, new QStandardItem(QString("TimeStamp")));

//    modelMacro = new QStandardItemModel( 0, 3 ,this );
//    //QStandardItem* item_en = new QStandardItem( QString("enable"));
//    QStandardItem* item_tx = new QStandardItem( QString("tx"));
//    QStandardItem* item_to = new QStandardItem( QString( "timeout"));
//    QStandardItem* item_cm = new QStandardItem( QString("comment"));
//    //modelMacro->setHorizontalHeaderItem( 0, item_en );
//    modelMacro->setHorizontalHeaderItem( 0, item_tx );
//    modelMacro->setHorizontalHeaderItem( 1, item_to );
//    modelMacro->setHorizontalHeaderItem( 2, item_cm );

    filterModelRx = new QSortFilterProxyModel(this);
    filterModelRx->setSourceModel( modelRx );

    ui->tableRx->setModel( filterModelRx );
    ui->tableRx->setColumnWidth(10,100);
    ui->tableMacro->setModel( macro->macroModel );

//    QList<QStandardItem*> riga;
//    item_en = new QStandardItem( true );
//    item_en->setCheckable(true);
//    riga.append( item_en );
//    riga.append( new QStandardItem( QString("ciao") ));
//    modelMacro->appendRow(riga);

    listaRx.clear();

    tick         = new QTimer(this);
    timerMacro   = new QTimer(this);
    timerRefresh = new QTimer(this);
    //timer1ms     = new QTimer(this);

    connect( ui->rb_10k,  SIGNAL(clicked(bool)), this, SLOT(setCanSpeed()) );
    connect( ui->rb_50k,  SIGNAL(clicked(bool)), this, SLOT(setCanSpeed()) );
    connect( ui->rb_100k, SIGNAL(clicked(bool)), this, SLOT(setCanSpeed()) );
    connect( ui->rb_125k, SIGNAL(clicked(bool)), this, SLOT(setCanSpeed()) );
    connect( ui->rb_250k, SIGNAL(clicked(bool)), this, SLOT(setCanSpeed()) );
    connect( ui->rb_500k, SIGNAL(clicked(bool)), this, SLOT(setCanSpeed()) );
    connect( ui->rb_1m,   SIGNAL(clicked(bool)), this, SLOT(setCanSpeed()) );
    connect( tick,        SIGNAL(timeout()),     this, SLOT(tickms())      );
    connect( timerMacro,  SIGNAL(timeout()),      this, SLOT(taskMacro())   );
    connect( timerRefresh, SIGNAL(timeout()),    this, SLOT(refreshRx())   );
    //connect( timer1ms,    SIGNAL(timeout()), this, SLOT(readData()) );

    //timer1ms->start(1);
    timerRefresh->start(50);

    ui->tableMacro->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableMacro->setSelectionMode(QAbstractItemView::SingleSelection);


//    char* buff = nullptr;

//    sprintf( buff, "%-10s", "as");

//    qDebug() << buff;

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_buttonOpenClosePort_clicked()
{
    seriale->setPortName( ui->comboPortName->itemText( ui->comboPortName->currentIndex()));
    seriale->setBaudRate( 115200 );
    seriale->setFlowControl(QSerialPort::NoFlowControl);
    seriale->setDataBits(QSerialPort::Data8);
    seriale->setParity(QSerialPort::NoParity);

    if( seriale->isOpen() == false )
    {        
        if( seriale->open(QIODevice::ReadWrite) )
        {
            rx.clear();
            flagRx   = 0;
            faseRead = 0;
            connect( seriale, SIGNAL(readyRead()), this , SLOT(readData()) );
            setCanSpeed();
            ui->buttonOpenClosePort->setText("Close");
            ui->buttonOpenClosePort->repaint();
            qDebug("port open");
        }
    }
    else
    {
        seriale->close();
        ui->buttonOpenClosePort->setText("Open");
        ui->buttonOpenClosePort->repaint();
        qDebug("port closed");
    }
}

void MainWindow::readData()
{
    char crx;
    while( seriale->bytesAvailable() )
    {
        seriale->read(&crx,1); // data.at(p);
        if( crx == '$' )
        {
            rx.clear();
        }
        else if( crx == '\r' )
        {
            procRx();
        }
        else
        {
            rx.append(crx);
        }
    }
}

void MainWindow::procRx()
{
    sCanMess mess;

    QString srx = QString( rx );

    mess.id = srx.mid( 0,3);
    mess.ln = srx.mid( 3,1);
    mess.b0 = srx.mid( 4,2);
    mess.b1 = srx.mid( 6,2);
    mess.b2 = srx.mid( 8,2);
    mess.b3 = srx.mid(10,2);
    mess.b4 = srx.mid(12,2);
    mess.b5 = srx.mid(14,2);
    mess.b6 = srx.mid(16,2);
    mess.b7 = srx.mid(18,2);

    int len = mess.ln.toInt();

    if( len < 8 ) mess.b7 = "  ";
    if( len < 7 ) mess.b6 = "  ";
    if( len < 6 ) mess.b5 = "  ";
    if( len < 5 ) mess.b4 = "  ";
    if( len < 4 ) mess.b3 = "  ";
    if( len < 3 ) mess.b2 = "  ";
    if( len < 2 ) mess.b1 = "  ";

    mess.current_ts = QTime::currentTime();

    listaRx.append( mess );

}

void MainWindow::refreshRx()
{
    while( !listaRx.isEmpty() )
    {
        QList<QStandardItem*> riga;

        sCanMess mess = listaRx.first();
        listaRx.removeFirst();

        QString ts = QString("%1:%2:%3.%4")
                .arg(mess.current_ts.hour(),2,10,QChar('0'))
                .arg(mess.current_ts.minute(),2,10,QChar('0'))
                .arg(mess.current_ts.second(),2,10,QChar('0'))
                .arg(mess.current_ts.msec(),3,10,QChar('0'));

        QStandardItem* item_id = new QStandardItem( mess.id);
        item_id->setBackground( QColor(205, 255, 100) );
        item_id->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter );

        QStandardItem* item_ln = new QStandardItem( mess.ln);
        item_ln->setBackground( QColor(100, 200, 255) );
        item_ln->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter );

        QStandardItem* item_b0 = new QStandardItem( mess.b0 );
        QStandardItem* item_b1 = new QStandardItem( mess.b1 );
        QStandardItem* item_b2 = new QStandardItem( mess.b2 );
        QStandardItem* item_b3 = new QStandardItem( mess.b3 );
        QStandardItem* item_b4 = new QStandardItem( mess.b4 );
        QStandardItem* item_b5 = new QStandardItem( mess.b5 );
        QStandardItem* item_b6 = new QStandardItem( mess.b6 );
        QStandardItem* item_b7 = new QStandardItem( mess.b7 );
        QStandardItem* item_ts = new QStandardItem( ts );

        item_b0->setBackground( QColor(255, 255, 150) );
        item_b1->setBackground( QColor(255, 255, 150) );
        item_b2->setBackground( QColor(255, 255, 150) );
        item_b3->setBackground( QColor(255, 255, 150) );
        item_b4->setBackground( QColor(255, 255, 150) );
        item_b5->setBackground( QColor(255, 255, 150) );
        item_b6->setBackground( QColor(255, 255, 150) );
        item_b7->setBackground( QColor(255, 255, 150) );

        item_b0->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter );
        item_b1->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter );
        item_b2->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter );
        item_b3->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter );
        item_b4->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter );
        item_b5->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter );
        item_b6->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter );
        item_b7->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter );
        item_ts->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter );

        item_ts->setSizeHint( QSize(80,20));

        riga.append( item_id );
        riga.append( item_ln );
        riga.append( item_b0 );
        riga.append( item_b1 );
        riga.append( item_b2 );
        riga.append( item_b3 );
        riga.append( item_b4 );
        riga.append( item_b5 );
        riga.append( item_b6 );
        riga.append( item_b7 );
        riga.append( item_ts );

        modelRx->insertRow( 0,riga );
    }
}


void MainWindow::on_buttonSendMess_clicked()
{
    if( seriale->isOpen() == false ) return;

    if(     ui->edit_id->text().length() == 1 ) ui->edit_id->setText( "00" + ui->edit_id->text() );
    else if(ui->edit_id->text().length() == 2 ) ui->edit_id->setText( "0"  + ui->edit_id->text() );
    else if(ui->edit_id->text().length() == 0 ) ui->edit_id->setText( "000" );

    if( ui->edit_b0->text().length() == 1 ) ui->edit_b0->setText( "0" + ui->edit_b0->text());
    if( ui->edit_b1->text().length() == 1 ) ui->edit_b1->setText( "0" + ui->edit_b1->text());
    if( ui->edit_b2->text().length() == 1 ) ui->edit_b2->setText( "0" + ui->edit_b2->text());
    if( ui->edit_b3->text().length() == 1 ) ui->edit_b3->setText( "0" + ui->edit_b3->text());
    if( ui->edit_b4->text().length() == 1 ) ui->edit_b4->setText( "0" + ui->edit_b4->text());
    if( ui->edit_b5->text().length() == 1 ) ui->edit_b5->setText( "0" + ui->edit_b5->text());
    if( ui->edit_b6->text().length() == 1 ) ui->edit_b6->setText( "0" + ui->edit_b6->text());
    if( ui->edit_b7->text().length() == 1 ) ui->edit_b7->setText( "0" + ui->edit_b7->text());

    if( ui->edit_len->text() == "9" ) ui->edit_len->setText( "8" );

    tx = ":" + ui->edit_id->text() + ui->edit_len->text() + ui->edit_b0->text() + ui->edit_b1->text() + ui->edit_b2->text() + ui->edit_b3->text() + ui->edit_b4->text() + ui->edit_b5->text() + ui->edit_b6->text() + ui->edit_b7->text() + "\r";

    if( ui->check_every->isChecked() )
    {
        tx_mem = tx;
        tick->setInterval( ui->edit_interval->text().toInt() );
        tick->start();
    }
    else
    {
        tx_mem = ui->edit_id->text() + " " +
                 ui->edit_len->text() + " " +
                 ui->edit_b0->text() + " " +
                 ui->edit_b1->text() + " " +
                 ui->edit_b2->text() + " " +
                 ui->edit_b3->text() + " " +
                 ui->edit_b4->text() + " " +
                 ui->edit_b5->text() + " " +
                 ui->edit_b6->text() + " " +
                 ui->edit_b7->text();

        int a = ui->combo_recent->findText(tx_mem);
        if( a < 0 )
        {
            ui->combo_recent->addItem( tx_mem );
        }
        seriale->write( tx.toLatin1().data() , tx.length() );
    }
}

void MainWindow::setCanSpeed()
{
    QString speed;

    if(      ui->rb_10k->isChecked() ) speed = "#10080700000000000000\r";
    else if( ui->rb_50k->isChecked() ) speed = "#10080600000000000000\r";
    else if( ui->rb_100k->isChecked()) speed = "#10080500000000000000\r";
    else if( ui->rb_125k->isChecked()) speed = "#10080400000000000000\r";
    else if( ui->rb_250k->isChecked()) speed = "#10080300000000000000\r";
    else if( ui->rb_500k->isChecked()) speed = "#10080200000000000000\r";
    else                               speed = "#10080100000000000000\r";

    seriale->write( speed.toLatin1() , speed.length() );

    ui->statusBar->showMessage( "Message speed changed..", 2000 );
}

void MainWindow::tickms()
{
    if( seriale->isOpen() )
    {
        seriale->write( tx_mem.toLatin1() , tx_mem.length() );
    }
}

void MainWindow::taskMacro()
{
    QItemSelectionModel *select = ui->tableMacro->selectionModel();

    if( select->hasSelection() == false )
    {
        //check if has selection
        ui->tableMacro->selectRow(0);
    }


    if( macro->current_index < macro->count() ) ++ macro->current_index;
    else macro->current_index = 0;

    ui->tableMacro->selectRow( macro->current_index );

    QModelIndexList selection = select->selectedRows();
    QModelIndex     isel = selection.at(0);

    int sel = isel.row();

    if( seriale->isOpen() )
    {

        QString macrotx    = ":" + macro->tx( sel );
        int     macrotimer = macro->timeout( sel );

        seriale->write( macrotx.toLatin1() , macrotx.length() );

        timerMacro->setInterval( macrotimer );

//        if( sel < ( macro->count() -1 ) ) ++sel;
//        else sel = 0;

//        ui->tableMacro->selectRow( sel );

    }
    else
    {
        timerMacro->stop();
    }
}

void MainWindow::on_check_every_clicked()
{
    if( ui->check_every->isChecked() == false )
    {
        tick->stop();
    }
}

void MainWindow::on_combo_recent_currentIndexChanged(const QString &arg1)
{
    QList<QString> mess = arg1.split(" ");

    ui->edit_id->setText(  mess[0] );
    ui->edit_len->setText( mess[1] );
    ui->edit_b0->setText(  mess[2] );
    ui->edit_b1->setText(  mess[3] );
    ui->edit_b2->setText(  mess[4] );
    ui->edit_b3->setText(  mess[5] );
    ui->edit_b4->setText(  mess[6] );
    ui->edit_b5->setText(  mess[7] );
    ui->edit_b6->setText(  mess[8] );
    ui->edit_b7->setText(  mess[9] );
}

void MainWindow::on_buttonClearRx_clicked()
{
    //modelRx->clear();
    modelRx->setRowCount(0);
}

void MainWindow::on_checkFilterId_clicked()
{
    if( ui->checkFilterId->isChecked() )
    {
        //filterModelRx->setFilterRegExp(QRegExp( ui->edit_filter_id->text() , Qt::CaseInsensitive, QRegExp::FixedString));
        filterModelRx->setFilterFixedString( ui->edit_filter_id->text() );
        filterModelRx->setFilterKeyColumn(0);
    }
    else
    {
        filterModelRx->setFilterRegExp(QRegExp( "" , Qt::CaseInsensitive, QRegExp::FixedString));
        filterModelRx->setFilterKeyColumn(0);
    }
}

void MainWindow::on_buttonOpenMacro_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Open file", QDir::currentPath() , "Macro (*.json )");
    macro->importa( fileName );
}

void MainWindow::on_buttonSaveMacro_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save file", QDir::currentPath() , "Macro (*.json )");
    macro->esporta( fileName );
}

void MainWindow::on_buttonAddToMacroList_clicked()
{
    if(     ui->edit_id->text().length() == 1 ) ui->edit_id->setText( "00" + ui->edit_id->text() );
    else if(ui->edit_id->text().length() == 2 ) ui->edit_id->setText( "0"  + ui->edit_id->text() );
    else if(ui->edit_id->text().length() == 0 ) ui->edit_id->setText( "000" );

    if( ui->edit_b0->text().length() == 1 ) ui->edit_b0->setText( "0" + ui->edit_b0->text());
    if( ui->edit_b1->text().length() == 1 ) ui->edit_b1->setText( "0" + ui->edit_b1->text());
    if( ui->edit_b2->text().length() == 1 ) ui->edit_b2->setText( "0" + ui->edit_b2->text());
    if( ui->edit_b3->text().length() == 1 ) ui->edit_b3->setText( "0" + ui->edit_b3->text());
    if( ui->edit_b4->text().length() == 1 ) ui->edit_b4->setText( "0" + ui->edit_b4->text());
    if( ui->edit_b5->text().length() == 1 ) ui->edit_b5->setText( "0" + ui->edit_b5->text());
    if( ui->edit_b6->text().length() == 1 ) ui->edit_b6->setText( "0" + ui->edit_b6->text());
    if( ui->edit_b7->text().length() == 1 ) ui->edit_b7->setText( "0" + ui->edit_b7->text());

    if( ui->edit_len->text() == "9" ) ui->edit_len->setText( "8" );

    tx = ui->edit_id->text() + ui->edit_len->text() + ui->edit_b0->text() + ui->edit_b1->text() + ui->edit_b2->text() + ui->edit_b3->text() + ui->edit_b4->text() + ui->edit_b5->text() + ui->edit_b6->text() + ui->edit_b7->text() + "\r";

    macro->add( tx, "1000" , "" );

}


void MainWindow::on_buttonStartMacro_clicked()
{
    if( macro->count() > 0 )
    {
        if( macro_run == true )
        {
            ui->buttonStartMacro->setText( "Start" );
            timerMacro->stop();
            macro_run = false;
        }
        else
        {
            ui->buttonStartMacro->setText( "Stop");

            macro->current_index = -1;
            timerMacro->start();
            macro_run = true;
        }
    }
}

void MainWindow::on_buttonClearMacro_clicked()
{
    timerMacro->stop();
    while( timerMacro->isActive() )
    {

    }
    macro_run = false;
    macro->clear();
}

void MainWindow::on_buttonReloadComPort_clicked()
{
    ui->comboPortName->clear();

    QList<QSerialPortInfo> listaCom = QSerialPortInfo::availablePorts();
    foreach ( QSerialPortInfo port, listaCom )
    {
        //if( port.isValid() )
        //{
            QString loc  = port.systemLocation();
            QString des  = port.description();
            QString nome = port.portName();
            ui->comboPortName->addItem(nome);
        //}
    }

}

void MainWindow::on_buttoSendRow_clicked()
{
    QItemSelectionModel *select = ui->tableMacro->selectionModel();

    if( select->hasSelection() == false )
    {
        //check if has selection
        ui->tableMacro->selectRow(0);
    }

    QModelIndexList selection = select->selectedRows();
    QModelIndex     isel = selection.at(0);

    int sel = isel.row();

    if( seriale->isOpen() )
    {

        QString macrotx    = ":" + macro->tx( sel );
        int     macrotimer = macro->timeout( sel );

        seriale->write( macrotx.toLatin1() , macrotx.length() );

        timerMacro->setInterval( macrotimer );

//        if( sel < ( macro->count() -1 ) ) ++sel;
//        else sel = 0;

//        ui->tableMacro->selectRow( sel );

    }
}

void MainWindow::on_buttonClearRow_clicked()
{
    if( macro->count() == 0) return;

    QItemSelectionModel *select = ui->tableMacro->selectionModel();

    if( select->hasSelection() == false )
    {
        //check if has selection
        ui->tableMacro->selectRow(0);
    }

    QModelIndexList selection = select->selectedRows();
    QModelIndex     isel = selection.at(0);

    int sel = isel.row();

    macro->remove( sel );

}
